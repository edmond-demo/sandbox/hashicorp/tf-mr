variable "vmname" {
  default = "CS-TF-Demo"
}
variable "region" {
  default = "us-east-1"
}
variable "access_key" {}
variable "secret_key" {}
variable "az" {
  default = "us-east-1a"
}
variable "ssh_key_name" {}
variable "vpc_cidr_block" {}
variable "dmz_cidr_block" {}
variable "main_instance_private_ip" {}
